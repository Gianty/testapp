﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SequenceGeneration
{
    public class SequenceGeneration
    {
        private readonly int _studentsCount;
        private readonly List<int> _limits;

        public SequenceGeneration(int studentsCount, IEnumerable<int> messagesLimits)
        {
            _studentsCount = studentsCount;
            _limits = ReturnLimitsList(studentsCount, messagesLimits);
        }

        public List<string> GenerateSequence()
        {
            if (_limits == null)
                return new List<string> { "-1" };

            var tuples = _limits.Select((value, i) => new Tuple<int, int>(i + 1, value)).ToList();
            var polycarp = tuples[0];
            tuples = tuples
                .Skip(1)
                .OrderByDescending(x => x.Item2)
                .ToList();
            tuples.Insert(0, polycarp);

            return GenerateOutput(tuples);
        }

        private List<string> GenerateOutput(List<Tuple<int, int>> list)
        {
            var output = new List<string>();
            var messagesSended = 0;
            var index = 0;

            while (messagesSended < _studentsCount - 1)
            {
                var current = list[index++];
                var sended = messagesSended;

                for (var j = messagesSended; j < Math.Min(sended + current.Item2, _studentsCount - 1); j++)
                {
                    output.Add($"{current.Item1}-->{list[j + 1].Item1}");
                    messagesSended++;
                }
            }
            output.Insert(0, messagesSended.ToString());

            return output;
        }

        private List<int> ReturnLimitsList(int studentsCount, IEnumerable<int> messagesLimits)
        {
            if (studentsCount < 1) throw new ArgumentException("Количество студентов должно быть положительным числом");
            var limits = messagesLimits as List<int> ?? messagesLimits.ToList();
            if (messagesLimits == null || limits.Count < studentsCount)
                throw new ArgumentException();

            var messagesCount = limits.Sum(count => count);
            if (messagesCount < studentsCount - 1)
                return null;
            if (limits[0] == 0 && studentsCount > 1)
                return null;

            return limits;
        }
    }
}
