﻿using System;
using System.Collections.Generic;
using FluentAssertions;
using NUnit.Framework;

namespace SequenceGeneration
{
    [TestFixture]
    public class Tests
    {
        private readonly List<string> _minusOneList = new List<string> { "-1" };
        private readonly List<int> _students = new List<int> { 4, 6, 3 };
        private readonly List<List<int>> _limits = new List<List<int>>
        {
            new List<int>{ 1, 2, 1, 0 },
            new List<int>{ 2, 0, 1, 3, 2, 0 },
            new List<int>{ 0, 2, 2 }
        };

        private readonly List<List<string>> _output = new List<List<string>>
        {
            new List<string>{ "3", "1-->2", "2-->3", "2-->4" },
            new List<string>{ "5", "1-->4", "1-->5", "4-->3", "4-->2", "4-->6" },
            new List<string>{ "-1" }
        };

        [Test]
        [TestCase(0, new[] { 0 }, TestName = "WhenStudentsCountLessThanOne")]
        [TestCase(5, null, TestName = "WhenLimitsEqualsNull")]
        [TestCase(2, new[] { 1 }, TestName = "WhenStudentsMoreThanLimitsCount")]
        public void ThrowsArgumentException(int students, int[] limits)
        {
            SequenceGeneration sg;
            Action test = () => sg = new SequenceGeneration(students, limits);
            test.ShouldThrow<ArgumentException>();
        }

        [Test]
        [TestCase(2, new[] { 0, 1 }, TestName = "FirstStudentMessagesCountEqualsZero")]
        [TestCase(3, new[] { 1, 0, 0 }, TestName = "PossibleMessagesCountLessThanStuduntsMinusOne")]
        public void ReturnMinusOne(int students, int[] limits)
        {
            var sg = new SequenceGeneration(students, limits);
            sg.GenerateSequence().Should().Equal(_minusOneList);
        }

        [Test]
        public void ReturnZero_WhenStudentsCountEqualsOne()
        {
            var sg = new SequenceGeneration(1, new List<int> { 0 });
            sg.GenerateSequence().Should().Equal(new List<string> { "0" });
        }

        [Test]
        public void ReturnRightEnum()
        {
            for (int i = 0; i < _students.Count; i++)
            {
                var sg = new SequenceGeneration(_students[i], _limits[i]);
                sg.GenerateSequence().Should().Equal(_output[i]);
            }
        }
    }
}