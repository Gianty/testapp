﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using TestTaskWebApp.Models;

namespace TestTaskWebApp.Controllers
{
    public class MainController : Controller
    {
        private Sequnce sequence;

        public MainController()
        {
            sequence = new Sequnce(){OutputList = new List<string>()};
        }

        // GET: Main
        public ActionResult Index()
        {
            return View(sequence);
        }

        [HttpPost]
        public ActionResult WriteSequence(string students, string limits)
        {
            int studentsCount;
            int limit;
            var stdudentsIsDigit = int.TryParse(students, out studentsCount);
            if (!stdudentsIsDigit)
            {
                sequence.OutputList = new List<string>{ "Количество студентов должно быть положительным числом" };
                return View("Index", sequence);
            }
            var messagesLimits = limits.Split(' ').Where(x => int.TryParse(x, out limit)).Select(int.Parse);
            try
            {
                var sg = new SequenceGeneration.SequenceGeneration(studentsCount, messagesLimits);
                var list = sg.GenerateSequence();
                list[0] = list[0].Insert(0, "Всего сообщений: ");
                sequence.OutputList = list;
                return View("Index", sequence);
            }
            catch (ArgumentException e)
            {
                sequence.OutputList = new List<string> { "Нужно указать ограничения для каждого студента через пробел" };
                return View("Index", sequence);
            }
        }
    }
}